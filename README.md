# README #

Este projeto é um estudo que apresenta de maneira simplificada as quatro possiveis implementações em uma aplicação gRPC.

* [Introdução](#markdown-header-introducao)
* [Passos para Execução](#markdown-header-passos-para-execução)
* [Tipos de gRPC](#markdown-header-tipos-de-grpc)
* [gRPC vs REST](#markdown-header-grpc-vs-rest)
* [Links Uteis](#markdown-header-links-uteis)

# Introdução

Como em muitos sistemas RPC, o gRPC é baseado na ideia de definir 
um serviço, especificando os métodos que podem ser chamados 
remotamente com seus parâmetros e tipos de retorno. 

No lado do servidor, o servidor implementa essa interface e executa
um servidor gRPC para lidar com as chamadas do cliente. 

No lado do cliente, o cliente tem um stub que fornece os mesmos 
métodos que o servidor.


### Passos para Execução ###

* Adjust Build and Run
    * Acessar o caminho abaixo:
  > Preferences > Build, Execution, Deployment > Build Tools > Gradle
    * Alterar para IntelliJ IDEA os seguintes campos:
        * Build and run using:
        * Run Tests using:
* Reload All Gradle Projects
  > Gradle > Reload All Gradle Projects
* Generate proto
    * Executar sempre que atualizar arquivos *.proto
  > Gradle > grpc-java > other > generateProto
* Executar servidor
  > com.cotrena.grpc.server > GreetingServer
* Executar cliente
  > com.cotrena.grpc.client > GreetingClient


### Tipos de gRPC ###

* Unary
    * Client envia um pedido
    * Servidor retorna uma resposta
* Server Streaming
    * Cliente envia um pedido
    * Servidor retorna várias respostas
* Client Streaming
    * Cliente envia vários pedidos
    * Servidor retorna uma resposta
* Bi Directional streaming
    * Cliente envia vários pedidos
    * Servidor envia várias respostas

### gRPC vs REST ###

| Feature                | gRPC                           | REST                          |
| ---------------------- | ------------------------------ | ----------------------------- |
| Contract               | Required (.proto)              | Optional (OpenAPI)            |
| Protocol               | HTTP/2                         | HTTP                          |
| Payload                | Protobuf (small, binary)       | JSON (large, human readable)  |
| Prescriptiveness       | Strict specification           | Loose. Any HTTP is valid      |
| Streaming              | Client, server, bi-directional | Client, server                |
| Browser support        | No (requires grpc-web)         | Yes                           |
| Security               | Transport (TLS)                | Transport (TLS)               |
| Client code-generation | Yes                            | OpenAPI + third-party tooling |

### Links Uteis ###

* [Quickstart](https://grpc.io/docs/languages/java/quickstart/)
* [Documentation](https://grpc.io/docs/)
* [Error Code](https://grpc.io/docs/guides/error/)
* [Deadline](https://grpc.io/blog/deadlines/)