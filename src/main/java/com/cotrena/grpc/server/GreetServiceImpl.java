package com.cotrena.grpc.server;

import com.proto.greet.*;
import io.grpc.stub.StreamObserver;

public class GreetServiceImpl extends GreetServiceGrpc.GreetServiceImplBase {
    @Override
    public void greet(GreetRequest request, StreamObserver<GreetResponse> responseObserver) {
        String firstName = request.getGretting().getFirstName();
        String lastName = request.getGretting().getLastName();

        GreetResponse response = GreetResponse.newBuilder()
                .setResult("first: " + firstName + " - last: " + lastName)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void greetManyTimes(GreetManyTimesRequest request, StreamObserver<GreetManyTimesResponse> responseObserver) {
        String firstName = request.getGretting().getFirstName();
        String lastName = request.getGretting().getLastName();
        String name = firstName + " " + lastName;

        try {
            for (int i=0; i<10; i++){
                GreetManyTimesResponse response = GreetManyTimesResponse.newBuilder()
                        .setResult("response number " + i + ": " + name)
                        .build();
                responseObserver.onNext(response);
                Thread.sleep(1000L);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            responseObserver.onCompleted();
        }
    }

    @Override
    public StreamObserver<LongGreetRequest> longGreet(StreamObserver<LongGreetResponse> responseObserver) {
        StreamObserver<LongGreetRequest> requestObserver = new StreamObserver<LongGreetRequest>() {
            StringBuilder result = new StringBuilder();

            @Override
            public void onNext(LongGreetRequest value) {
                result.append("Hello " + value.getGretting().getFirstName() + "! ");
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("err: " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                responseObserver.onNext(
                        LongGreetResponse.newBuilder()
                                .setResult(result.toString())
                                .build()
                );
                responseObserver.onCompleted();
            }
        };
        return requestObserver;
    }

    @Override
    public StreamObserver<GreetEveryoneRequest> greetEveryone(StreamObserver<GreetEveryoneResponse> responseObserver) {
        StreamObserver<GreetEveryoneRequest> requestObserver = new StreamObserver<GreetEveryoneRequest>() {
            StringBuilder result = new StringBuilder();

            @Override
            public void onNext(GreetEveryoneRequest value) {
                String result = "Hello " + value.getGretting().getFirstName();
                responseObserver.onNext(
                        GreetEveryoneResponse.newBuilder()
                                .setResult(result)
                                .build()
                );
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("err: " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
        return requestObserver;
    }
}
