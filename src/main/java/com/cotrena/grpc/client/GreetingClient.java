package com.cotrena.grpc.client;

import com.proto.greet.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class GreetingClient {
    public static void main(String[] args) {
        System.out.println("Hello gRPC Client");

        GreetingClient client = new GreetingClient();
        client.run();
    }

    private void run() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051)
                .usePlaintext()
                .build();

        // doUnaryCall(channel);
        // doServerStreamingCall(channel);
        // doClientStreamingCall(channel);
        doBiDirectionalStreamingCall(channel);
        System.out.println("Shutting down channel");
        channel.shutdown();
    }

    private void doUnaryCall(ManagedChannel channel) {
        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);

        Greeting gretting = Greeting.newBuilder()
                .setFirstName("Cristian")
                .setLastName("Cotrena")
                .build();

        GreetRequest request = GreetRequest.newBuilder()
                .setGretting(gretting)
                .build();

        GreetResponse greetResponse = greetClient.greet(request);
        System.out.println(greetResponse.getResult());
    }

    private void doServerStreamingCall(ManagedChannel channel) {
        GreetServiceGrpc.GreetServiceBlockingStub greetClient = GreetServiceGrpc.newBlockingStub(channel);

        Greeting gretting = Greeting.newBuilder()
                .setFirstName("Cristian")
                .setLastName("Cotrena")
                .build();

        GreetManyTimesRequest request = GreetManyTimesRequest.newBuilder()
                .setGretting(gretting)
                .build();

        greetClient.greetManyTimes(request).forEachRemaining(greetManyTimesResponse -> {
            System.out.println(greetManyTimesResponse.getResult());
        });
    }

    private void doClientStreamingCall(ManagedChannel channel) {
        GreetServiceGrpc.GreetServiceStub asyncClient = GreetServiceGrpc.newStub(channel);
        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<LongGreetRequest> requestObserver =
                asyncClient.longGreet(new StreamObserver<LongGreetResponse>() {
            @Override
            public void onNext(LongGreetResponse value) {
                System.out.println("Receive a response from the server.");
                System.out.println(value.getResult());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("err: " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                System.out.println("Server has completed sending us something");
                latch.countDown();
            }
        });

        for (int i=0; i<3; i++) {
            System.out.println("send");
            requestObserver.onNext(
                    LongGreetRequest.newBuilder()
                            .setGretting(Greeting.newBuilder()
                                    .setFirstName("name" + i)
                                    .setLastName("subname" + i)
                                    .build()
                            )
                            .build());
        }
        requestObserver.onCompleted();

        try {
            latch.await(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doBiDirectionalStreamingCall(ManagedChannel channel) {
        GreetServiceGrpc.GreetServiceStub asyncClient = GreetServiceGrpc.newStub(channel);

        CountDownLatch latch = new CountDownLatch(1);

        StreamObserver<GreetEveryoneRequest> requestObserver =
                asyncClient.greetEveryone(new StreamObserver<GreetEveryoneResponse>() {
            @Override
            public void onNext(GreetEveryoneResponse value) {
                System.out.println("Response from server " + value.getResult());

            }

            @Override
            public void onError(Throwable t) {
                latch.countDown();
            }

            @Override
            public void onCompleted() {
                System.out.println("Server is done sendind data.");
                latch.countDown();
            }
        });

        Arrays.asList("Alice", "Patricia", "Pedro", "Luana").forEach(
                name -> requestObserver.onNext(GreetEveryoneRequest.newBuilder()
                        .setGretting(Greeting.newBuilder()
                                .setFirstName(name)
                                .build())
                        .build()
                )
        );

        requestObserver.onCompleted();

        try {
            latch.await(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
